Hello!
Thank you for the interesting task.
In this document I will write comments to the work that I did.


1. Asynchrony for animation.
In this task, I made an asynchronous movement for jumping spheres.
Using a small script with the randomization function, I set the asynchrony for animation (Jumping).
That allows you to get the desired result in a short time, and the script consists of one line of code that works very quickly and efficiently.
Also in this script, there is a code that lets you set an indent force for each of the spheres on the scene.

2. Optimization for the shader.
In the shader, I deleted the circuits that didn’t do anything,
and the correct variables were declared to speed up the shader calculations.

I transferred the information from the fragment shader to the vertex shader,
which makes the shader to work much faster and process the information not with a processor, but with a video card.
I also deleted the pass with the code for the shadow and replaced with the line that calls this pass from the VertexLit shader.

3. Adding a shadow to the spheres.
To add shadows for some spheres, I added a line of code to invoke the shadow pass from the VertexLit shader.

4. Special effect.
I created a special touchdown effect using Unity's particle system. The effect itself combined in Unity.
However, Houdini program was used to generate an explosion-like effect procedurally.
Changing the parameters in Houdini for effect, you can get completely different effects for the same asset.
Houdini is exceptional for creating a variety.


5. Procedural wall.
To create a procedural wall, I used the Houdini program.
The wall is entirely procedural. At any time the asset allows you to change parameters for the wall and set up its parameters according to requirements.
In the scene, there are 2 options for working with the wall.
The first allows you to play around with the parameters of generating, and the second allows you to put blocks of walls along an arbitrary curve.
You can play with the settings of the asset directly in the Houdini.
Also in the asset, there is a system for automatically assigning materials if necessary.
