﻿Shader "Unlit/Ground"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
		    Tags {"LightMode"="ForwardBase"}
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase
			 
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"

			struct appdata
			{
				float4 pos : POSITION;				
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
				
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
			    SHADOW_COORDS(1)
				float4 pos : SV_POSITION;
				float3 normal : NORMAL;
				fixed3 ambient : TEXCOORD2;
				fixed3 diff : TEXCOORD3;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				
				o.pos = UnityObjectToClipPos(v.pos);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.normal = v.normal;

				float3 worldNormal = UnityObjectToWorldNormal(v.normal);
				o.ambient = ShadeSH9(float4(worldNormal,1));
				float3 nl = max(0, dot(worldNormal, _WorldSpaceLightPos0));
				o.diff = nl.g * _LightColor0;
				TRANSFER_SHADOW(o)
				return o;
			}
			
			float4 frag (v2f i) : SV_Target
			{
			    float4 col;
				float3 shadow, lighting;
			    col = tex2D(_MainTex, i.uv);
                shadow = SHADOW_ATTENUATION(i);
                lighting = i.diff * shadow + i.ambient;
                col.rgb *= lighting.rgb;
				return col;
			}
			ENDCG
		}
		UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
        
	}
}
